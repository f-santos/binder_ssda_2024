Dépôt "de secours" pour l'école d'été d'Archéologie digitale
============================================================
**Session Rstudio distante** : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/f-santos%2Fbinder_ssda_2024/HEAD)

Cé dépôt permettra de réaliser directement en ligne les analyses du TP, pour les étudiants n'ayant pas pu installer correctement les logiciels sur leur poste.

Rappel du site web du TP : [https://f-santos.gitlab.io/summer-school-morpho](https://f-santos.gitlab.io/summer-school-morpho).
